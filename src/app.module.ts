import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import * as ormconfig from './ormconfig';
import { AuthModule } from './auth/auth.module';
import {AuthMiddleware} from "./middlewares/auth.middleware";
import { AdminModule } from './admin/admin.module';
import {AdminMiddleware} from "./middlewares/admin.middleware";

@Module({
  imports: [TypeOrmModule.forRoot(ormconfig), AuthModule, AdminModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AuthMiddleware).exclude({ path: 'auth/logout', method: RequestMethod.GET }).forRoutes('auth')
    consumer.apply(AdminMiddleware).exclude({ path: 'admin/addOrder',method: RequestMethod.POST}).forRoutes('admin')
  }

}

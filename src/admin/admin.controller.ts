import {Body, Controller, Get, HttpStatus, Post, Render, Res, ValidationPipe} from '@nestjs/common';
import {AdminService} from "./admin.service"
import {OrderCreateDto} from "../dto/orderCreate.dto";

@Controller('admin')
export class AdminController {
    constructor(private readonly adminService: AdminService) {}

    @Get()
    @Render('admin/index')
    async showIndex(): Promise<object> {
        const data = await this.adminService.getAll();
        return {message: 'Admin Page',data: data}
    }

    @Post('addOrder')
    async AddOrder(@Res() res,@Body(ValidationPipe) orderCreateDto: OrderCreateDto){
        const order =  await this.adminService.save(orderCreateDto)
        if(order){
            res.status(HttpStatus.OK).send();
        }
    }

}

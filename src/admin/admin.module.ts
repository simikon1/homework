import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Orders} from "./order.entity";


@Module({
  imports: [TypeOrmModule.forFeature([Orders])],
  controllers: [AdminController],
  providers: [AdminService]
})
export class AdminModule {}

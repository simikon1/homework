import { Injectable } from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Orders} from "./order.entity";
import {OrderCreateDto} from "../dto/orderCreate.dto";

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(Orders)
        private ordersRepository: Repository<Orders>
    ) {
    }

    async getAll(){
        return await this.ordersRepository.createQueryBuilder().select("orders").from(Orders, "orders").getMany();
    }

    async save(orderCreateDto: OrderCreateDto) {
        return await this.ordersRepository.save(orderCreateDto);
    }
}

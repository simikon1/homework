import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";

export class Orders1600891394467 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
           name: 'orders',
           columns: [
               new TableColumn({
                   name: 'id',
                   type: "integer",
                   isPrimary: true,
                   generationStrategy: "increment",
                   isGenerated: true
               }),
               new TableColumn({
                   name: 'phone',
                   type: 'varchar',
                   isNullable: false
               }),
               new TableColumn({
                   name: 'destination',
                   type: 'varchar',
                   isNullable: false
               }),
           ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('orders')
    }

}

import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";

export class Users1600974366749 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'users',
            columns: [
                new TableColumn({
                    name: 'id',
                    type: "integer",
                    isPrimary: true,
                    generationStrategy: "increment",
                    isGenerated: true
                }),
                new TableColumn({
                    name: 'name',
                    type: 'varchar',
                    isNullable: false
                }),
                new TableColumn({
                    name: 'email',
                    type: 'varchar',
                    isNullable: false,
                    isUnique: true
                }),
                new TableColumn({
                    name: 'password',
                    type: 'varchar',
                    isNullable: false
                }),
                new TableColumn({
                    name: 'role',
                    type: 'varchar',
                    isNullable: true
                }),
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('users')
    }

}

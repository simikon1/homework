import {Body, Controller, Get, HttpStatus, Post, Render, Req, Res, Session, ValidationPipe} from '@nestjs/common';
import {AuthService} from "./auth.service";
import {UserCreateDto} from "../dto/userCreate.dto";
import {UserLoginDto} from "../dto/userLogin.dto";

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Get('login')
    @Render('auth/login')
    ShowLogin(@Req() req): object {
        return {message: 'Login Page'}
    }

    @Get('register')
    @Render('auth/register')
    ShowRegister(): object {
        return {message: 'Register Page'}
    }

    @Post('register')
    async Register(@Res() res,@Body(ValidationPipe) userCreateDto: UserCreateDto) {
        const user =  await this.authService.save(userCreateDto)
        if(user){
            res.status(HttpStatus.OK).send();
        }
    }

    @Post('login')
    async Login(@Res() res,@Req() req,@Body(ValidationPipe) userLoginDto: UserLoginDto, @Session() session) {
        const user = await this.authService.login(userLoginDto);
        if(user) {
            session.user = user
            session.userId = user.id
            session.userSid = req.sessionID;
            res.cookie('userSid', req.sessionID);
            session.save();
            return res.redirect('/');
        }
    }

    @Get('logout')
    async Logout(@Res() res,@Req() req) {
        if (req.session.userSid && req.cookies.userSid) {
            res.clearCookie('userSid');
            res.redirect('/');
        } else {
            res.redirect('/auth/login');
        }
    }
}

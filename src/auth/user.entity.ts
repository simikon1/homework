import {Entity, Column, PrimaryGeneratedColumn, BaseEntity, Unique} from 'typeorm';

@Entity()
@Unique(['email'])
export class Users extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({ default: 'user' })
    role: string;

    async validatePassword(password: string): Promise<boolean> {
        const bcrypt = require('bcrypt');
        return !!bcrypt.compare(password, this.password);
    }
}
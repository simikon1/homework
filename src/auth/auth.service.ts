import {Injectable} from "@nestjs/common";
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {Users} from "./user.entity";
import {UserCreateDto} from "../dto/userCreate.dto";
import {UserLoginDto} from "../dto/userLogin.dto";


@Injectable()
export class AuthService {
    private saltRounds = 10

    constructor(
        @InjectRepository(Users)
        private usersRepository: Repository<Users>
    ) {
    }

    async save(userCreateDto: UserCreateDto) {
        const bcrypt = require('bcrypt');
        userCreateDto.password = await bcrypt.hash(userCreateDto.password, this.saltRounds)
        return await this.usersRepository.save(userCreateDto);
    }

    async find(userLoginDto: UserLoginDto) {
        return await this.usersRepository.findOne(userLoginDto);
    }

    async login(userLoginDto: UserLoginDto) {
        const {email, password} = userLoginDto

        const user = await this.usersRepository.findOne({email})
        if (user) {
            if(await user.validatePassword(password)){
                return user
            } else {
                console.log(false);
            }
        }

    }
}
import {IsNotEmpty, IsNumberString, IsString} from 'class-validator'

export class OrderCreateDto {
    @IsNotEmpty()
    @IsNumberString()
    phone: string
    
    @IsNotEmpty()
    @IsString()
    destination: string
}
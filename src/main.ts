import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from "@nestjs/platform-express";
import { join } from 'path';
import {ValidationPipe} from "@nestjs/common";
import * as cookieParser from 'cookie-parser'
import * as session from 'express-session';
import {MainMiddleware} from "./middlewares/main.middleware";
const redis = require('redis')
const redisStore = require('connect-redis')(session)
const redisClient = redis.createClient()

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  redisClient.on('connect', function(){
    console.log('Connected to Redis');
  });

  redisClient.on('error', function(err) {
    console.log('Redis error: ' + err);
  });
  app.use(cookieParser());
  app.use(session({
    name: 'userSid',
    store: new redisStore({ client: redisClient }),
    secret: '1234567890QWERT',
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 1000 * 60 * 60 * 2,
      sameSite: true,
    }
  }))
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('pug');
  app.use(MainMiddleware);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(8000);
}
bootstrap();

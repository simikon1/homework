import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class AdminMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: Function) {
        if (res.locals.user) {
            if(res.locals.user.role === 'admin'){
                next();
            } else {
                return res.redirect('/');
            }
        } else {
            return res.redirect('/');
        }
    }
}

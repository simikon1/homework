import { Request, Response } from 'express';

export function MainMiddleware(req: Request, res: Response, next: Function)  {
    if (req.session.user) {
        res.locals.user = req.session.user
    }
    next();
}

import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: Function) {
        if (req.cookies.userSid && req.session.userId) {
            return res.redirect('/');
        }
        next();
    }
}

$(document).ready(function (){
    $('form#register').submit(function (e){
        e.preventDefault();
        let form = $(this).serializeArray();
        $.ajax({
            type: "POST",
            url: '/auth/register',
            data: form,
            dataType: 'json'
        }).always(function (response){
            if(response.status === 200){
                location.href = '/auth/login';
            } else {
                $('#errors').addClass('alert-danger');
                $('#errors').text(response.responseJSON.message)
            }
        });
    });

    $('form#orders').submit(function (e){
        e.preventDefault();
        let form = $(this).serializeArray();
        $.ajax({
            type: "POST",
            url: '/admin/addOrder',
            data: form,
            dataType: 'json'
        }).always(function (response){
            if(response.status === 200){
                location.href = '/';
            } else {
                $('#errors').addClass('alert-danger');
                $('#errors').text(response.responseJSON.message)
            }
        });
    });


});